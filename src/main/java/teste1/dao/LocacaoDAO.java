package teste1.dao;

import teste1.entidades.Locacao;

import java.util.List;

public interface LocacaoDAO {
    public void salvar(Locacao locacao);

    List<Locacao> obterLocacoesPendentes();
}
