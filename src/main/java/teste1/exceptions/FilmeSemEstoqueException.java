package teste1.exceptions;

public class FilmeSemEstoqueException extends Exception {
    private static final long serialVersionUID = -4970527916966267734L;

    public FilmeSemEstoqueException(String message) {
        super(message);
    }

}
