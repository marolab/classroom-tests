package teste1.exceptions;

public class DivisaoPorZeroException extends Exception{
    private static final long serialVersionUID = -4970527916966267734L;
    public DivisaoPorZeroException(String message) {
        super(message);
    }
}
