package teste1.utils;

public enum ErrorMessage {

    MSG_ERROR_FILME_SEM_ESTOQUE("Filme não está em estoque!"),
    MSG_FAIL_TEST("Exceção não ocorreu, teste falhou!"),
    MSG_ERROR_FILME_NAO_INFORMADO("Filme não informado!"),
    MSG_ERROR_USUARIO_NAO_INFORMADO("Usuário não informado!"),
    MSG_ERROR_DIVISAO_POR_ZERO("Não possível divisão por zero!"),
    MSG_ERROR_USUARIO_NEGATIVADO("Usuário negativado, não poderá fazer a locação!"),
    MSG_ERROR_SPC_FORA_DO_AR("Sistema SPC não respondeu");

    private String mensagem;

    ErrorMessage(String mensagem) {
        this.mensagem = mensagem;
    }

    String getMensagem() {
        return mensagem;
    }

}
