package teste1.entidades;

public class Locadora {
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Locadora(String nome) {
        this.nome = nome;
    }
}
