package teste1.servicos;

import teste1.exceptions.DivisaoPorZeroException;
import teste1.utils.CalculadorSomaTDD;
import teste1.utils.ErrorMessage;

public class CalculadoraService {
    private CalculadorSomaTDD calculadorSomaTDD;

    public int somar(CalculadorSomaTDD calculadorSomaTDD) {
        return calculadorSomaTDD.getA() + calculadorSomaTDD.getB();
    }

    public int somar(Integer a, Integer b) {
        System.out.println("Somando...");
        return a + b;
    }

    public int subtrair(CalculadorSomaTDD calculadorSomaTDD) {
        return calculadorSomaTDD.getA() - calculadorSomaTDD.getB();
    }

    public int subtrair(Integer a, Integer b) {
        System.out.println("Subtraindo...");
        return a - b;
    }

    public int dividir(CalculadorSomaTDD calculadorSomaTDD) throws DivisaoPorZeroException {
        if(calculadorSomaTDD.getB() == 0) {
            throw new DivisaoPorZeroException(ErrorMessage.MSG_ERROR_DIVISAO_POR_ZERO.toString());
        }
        return calculadorSomaTDD.getA() / calculadorSomaTDD.getB();
    }

    public void imprimeTest() {
        System.out.println("Executou o método!");
    }
}
