package teste1.servicos;

import teste1.dao.LocacaoDAO;
import teste1.entidades.Filme;
import teste1.entidades.Locacao;
import teste1.entidades.Usuario;
import teste1.exceptions.FilmeSemEstoqueException;
import teste1.exceptions.LocadoraException;
import teste1.utils.DataUtilsLc;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static teste1.utils.DataUtilsLc.adicionarDias;
import static teste1.utils.ErrorMessage.*;

public class LocacaoService {

    private LocacaoDAO locacaoDAO;
	private SPCService spcService;
	private EmailService emailService;

    public LocacaoService() { }

    public Locacao alugarFilme(Usuario usuario, List<Filme> filmes) throws FilmeSemEstoqueException, LocadoraException {

        if(usuario == null) {
            throw new LocadoraException(MSG_ERROR_USUARIO_NAO_INFORMADO.toString());
        }
        if(filmes == null || filmes.isEmpty()) {
            throw new LocadoraException(MSG_ERROR_FILME_NAO_INFORMADO.toString());
        }
        for(Filme filme: filmes) {
            if (filmes.get(0).getEstoque() == 0) {
                throw new FilmeSemEstoqueException(MSG_ERROR_FILME_SEM_ESTOQUE.toString());
            }
        }
        boolean negativado;
        try {
            negativado = spcService.possuiNegativacao(usuario);
        } catch (Exception e) {
            throw new LocadoraException(MSG_ERROR_SPC_FORA_DO_AR.toString());

        }

        if (negativado) {
            throw new LocadoraException(MSG_ERROR_USUARIO_NEGATIVADO.toString());
        }

        Locacao locacao = new Locacao();
        locacao.setFilme(filmes);
        locacao.setUsuario(usuario);
        locacao.setDataLocacao(new Date());

        locacao.setValor(calcularDescontoLocacao(filmes));
        //Entrega no dia seguinte
        Date dataEnDate = new Date();
        dataEnDate = adicionarDias(dataEnDate, 1);
        if(DataUtilsLc.verificarDiaSemana(dataEnDate, Calendar.SUNDAY)) {
            dataEnDate = adicionarDias(dataEnDate, 1);
        }

        locacao.setDataRetorno(dataEnDate);

        locacaoDAO.salvar(locacao);

        return locacao;
    }


    private Double calcularDescontoLocacao(List<Filme> filmes) {
        Double valorTotal = 0.0D;
        int size = filmes.size();
        int i = 0;

        Double valorFilme;
        for (Iterator var5 = filmes.iterator(); var5.hasNext(); valorTotal = valorTotal + valorFilme) {
            Filme filme = (Filme) var5.next();
            valorFilme = ((Filme) filmes.get(i)).getPrecoLocacao();
            ++i;
            if (i == size) {
                switch (size) {
                    case 3:
                        valorFilme = valorFilme * 0.75D;
                        break;
                    case 4:
                        valorFilme = valorFilme * 0.5D;
                        break;
                    case 5:
                        valorFilme = valorFilme * 0.25D;
                        break;
                    default:
                        if (size > 5) {
                            valorFilme = valorFilme * 0.0D;
                        }
                }
            }
        }

        return valorTotal;
    }
    public void notificarAtrasos(){
        List<Locacao> locacoes = locacaoDAO.obterLocacoesPendentes();
        for(Locacao locacao: locacoes) {
            // somente as locações atrasadas
            if(locacao.getDataRetorno().before(new Date())) {
                emailService.notificarAtraso(locacao.getUsuario());
            }
        }
    }

    public void prorrogarLocacao(Locacao oldLocacao, int dias) {
        Locacao locacao = new Locacao();
        locacao.setUsuario(oldLocacao.getUsuario());
        locacao.setFilme(oldLocacao.getFilme());
        locacao.setValor(oldLocacao.getValor() * dias);
        locacao.setDataLocacao(new Date());
        locacao.setDataRetorno(DataUtilsLc.obterDataComDiferencaDias(dias));
        locacaoDAO.salvar(locacao);
    }


/*
    Uma vez que usamos @INJECTMOCK e @MOCK não se faz mais necessária estes métodos para injeção
    pois o Framework cuidará disto

    // set para injeção de dependência
    public void setLocacaoDAO(LocacaoDAO locacaoDAO) {
        this.locacaoDAO = locacaoDAO;
    }
    // set para injeção de dependência
	public void setSPCService(SPCService spc) {
		this.spcService = spc;
	}
	// set para injeção de dependêncai
    public void setEmailService(EmailService emailService) { this.emailService = emailService;}

 */
}