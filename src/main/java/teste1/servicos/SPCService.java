package teste1.servicos;

import teste1.entidades.Usuario;

public interface SPCService {
    public boolean possuiNegativacao(Usuario usuario) throws Exception;
}
