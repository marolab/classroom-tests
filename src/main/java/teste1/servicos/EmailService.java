package teste1.servicos;

import teste1.entidades.Usuario;

public interface EmailService {
    public void notificarAtraso(Usuario usuario);
}
