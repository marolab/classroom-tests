package teste1.builders;

import teste1.entidades.Filme;
import teste1.entidades.Locacao;
import teste1.entidades.Usuario;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static teste1.utils.DataUtilsLc.obterDataComDiferencaDias;

public class LocacaoBuilder {
    private Locacao locacao;
    private LocacaoBuilder() {}

    public static LocacaoBuilder LocacaoBuilder() {
        LocacaoBuilder builder = new LocacaoBuilder();
        builder.locacao = new Locacao();
        List<Filme> filmeList = Arrays.asList(FilmeBuilder.filmeBuilder().getFilme(), FilmeBuilder.filmeBuilder().getFilme());
        builder.locacao.setFilme(filmeList);
        builder.locacao.setDataLocacao(new Date());
        builder.locacao.setDataRetorno(obterDataComDiferencaDias(2));
        builder.locacao.setUsuario(UsuarioBuilder.usuarioBuilder().getUsuario());
        builder.locacao.setValor(4d);
        return builder;
    }

    public Locacao getLocacao(){
        return this.locacao;
    }

    public LocacaoBuilder getDataLocacaoVencida() {
        locacao.setDataRetorno(obterDataComDiferencaDias(-2));
        return this;
    }

    public LocacaoBuilder comUsuario(Usuario usuario) {
        locacao.setUsuario(usuario);
        return this;
    }

    public LocacaoBuilder atrasados() {
        locacao.setDataLocacao(obterDataComDiferencaDias(-4));
        locacao.setDataRetorno(obterDataComDiferencaDias(-2));
        return this;
    }

}
