package teste1.builders;

import teste1.entidades.Usuario;

public class UsuarioBuilder {

    private Usuario user;

    private UsuarioBuilder(){}

    public static UsuarioBuilder usuarioBuilder() {
        UsuarioBuilder userBuilder = new UsuarioBuilder();
        userBuilder.user = new Usuario("Lochas");
        return userBuilder;
    }

    public Usuario getUsuario() {
        return user;
    }

    public UsuarioBuilder comNome(String name) {
        user.setNome(name);
        return this;
    }

}
