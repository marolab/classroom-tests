package teste1.builders;

import teste1.entidades.Filme;

public class FilmeBuilder {

    private Filme filme;

    private FilmeBuilder() {}

    public static FilmeBuilder filmeBuilder() {
        FilmeBuilder builder = new FilmeBuilder();
        builder.filme = new Filme();
        builder.filme.setNome("A volta dos que não foram");
        builder.filme.setEstoque(1);
        builder.filme.setPrecoLocacao(4d);
        return builder;
    }

    public static FilmeBuilder filmeBuilderSemEstoque() {
        FilmeBuilder builder = new FilmeBuilder();
        builder.filme = new Filme();
        builder.filme.setNome("A volta dos que não foram");
        builder.filme.setEstoque(0);
        builder.filme.setPrecoLocacao(4d);
        return builder;
    }

    public Filme getFilme() {
        return filme;
    }

    public FilmeBuilder getFilmeSemEstoque(){
        filme.setEstoque(0);
        return this;
    }

    public FilmeBuilder getFilmeSemValor() {
        filme.setPrecoLocacao(0d);
        return this;
    }

}
