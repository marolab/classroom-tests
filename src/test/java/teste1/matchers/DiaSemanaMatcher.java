package teste1.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import teste1.utils.DataUtilsLc;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

// o tipo generics (<T>) informado ao estender a SuperClass, deverá para o primeiro parâmetro a usar no matcher
public class DiaSemanaMatcher extends TypeSafeMatcher<Date> {

    private Integer diaSemana;


    // construtor recebe um parâmetro conforme a lógica do matcher definida
    public DiaSemanaMatcher(Integer diaSemana) {
        this.diaSemana = diaSemana;
    }
    // este é o método Match, que fará a comparação e resultará verdadeiro ou falso
    @Override
    protected boolean matchesSafely(Date date) {
        return DataUtilsLc.verificarDiaSemana(date, diaSemana);
    }

    // este método DESCRIBETO, é responsável por informar a mensagem do valor esperado
    // para a comparação do Matcher.
    @Override
    public void describeTo(Description description) {
        /* neste exemplo foi pego o dia da semana atual */
        Calendar dia = Calendar.getInstance();
        dia.set(Calendar.DAY_OF_WEEK, diaSemana);
        description.appendText(dia.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, new Locale("pt", "BR")));

    }
}
