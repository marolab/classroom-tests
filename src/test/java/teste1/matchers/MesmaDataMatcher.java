package teste1.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import teste1.utils.DataUtilsLc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MesmaDataMatcher extends TypeSafeMatcher<Date> {
    private Integer qtDias;

    public MesmaDataMatcher(Integer qtDias) {
        this.qtDias = qtDias;
    }

    @Override
    protected boolean matchesSafely(Date date) {
        return DataUtilsLc.isMesmaData(date, DataUtilsLc.obterDataComDiferencaDias(qtDias));
    }

    @Override
    public void describeTo(Description description) {
        Date dataEsperada = DataUtilsLc.obterDataComDiferencaDias(qtDias);
        DateFormat format = new SimpleDateFormat("dd/MM/YYYY");
        description.appendText(format.format(dataEsperada));
    }
}
