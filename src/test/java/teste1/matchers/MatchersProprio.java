package teste1.matchers;

import java.util.Calendar;
import java.util.Date;

public class MatchersProprio {

    public static DiaSemanaMatcher caiEm(Integer dia) {
       return new DiaSemanaMatcher(dia);
    }

    public static DiaSemanaMatcher caiNumaSegunda() {
       return new DiaSemanaMatcher(Calendar.MONDAY);
    }

    public static MesmaDataMatcher ehHoje() {
        return new MesmaDataMatcher(0);
    }

    public static MesmaDataMatcher ehHojeComDiferencaDias(Integer dias) {
        return new MesmaDataMatcher(dias);
    }

}
