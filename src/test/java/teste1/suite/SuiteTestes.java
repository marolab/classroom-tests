package teste1.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import teste1.servicos.CalculoDescontoLocacaoParametrized;
import teste1.servicos.LocacaoServiceTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CalculoDescontoLocacaoParametrized.class,
        LocacaoServiceTest.class
})
public class SuiteTestes {
    // suite executará todos os métodos das classes anotadas acima.
}
