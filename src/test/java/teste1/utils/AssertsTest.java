package teste1.utils;

import teste1.entidades.Locadora;
import teste1.entidades.Usuario;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

public class AssertsTest {
    @Test
    public void test() {
        // para validar igualdades (o método somente validará pelo que espera, evitando o uso de negativas.
        assertTrue(true);
        assertFalse(false);

        // comparando igualdades AssertEquals
        // LONG - INTEGER - comparando valores numéricos, como long e inteiros
        int i = 5;
        Integer I = 5;
        // apesar de serem o mesmo tipo raiz, serão compreendidos como tipos diferentes
        // para comparar será necessário usar motodos de wapper ou parse.
        assertEquals(i, I.intValue());
        assertEquals(Integer.valueOf(i), I);

        // DOUBLE - FLOAT
        // para comparar valores flutuantes, faz-se necessário o uso do terceiro parâmetro, um Delta
        // o qual define a tolerância da comparação.
        assertEquals(Math.PI, 3.14, 0.01);
        assertNotEquals(4.3211, 4.32, 0.001);

        // STRING
        // ao trabalhar com String, o Asser leva em consideração sensitiveCase
        String a = "bola";
        String b = "Bola";
        assertNotEquals(a, b);
        // ou utilizando métodos da classe String para parse e será utilizado AssertTrue
        assertTrue(a.equalsIgnoreCase(b));

        // OBJETOS
        // ao comparar objetos, será utilizaro o método Equals da classe testada.
        // de maneira, se não existir na classe, o Java percorrerá as super-class do mesmo
        // chegando atá Object, a qual apenas testa se a Instância do objeto é a mesma.

        // objeto com Equals
        Usuario u1 = new Usuario("Alôncio");
        Usuario u2 = new Usuario("Alôncio");
        Usuario u3 = null;
        Usuario u4 = u1;

        assertEquals(u1, u2);
        // objeto sem Equals
        Locadora l1 = new Locadora("Montenegro");
        Locadora l2 = new Locadora("Montenegro");
        assertNotEquals(l1, l2);

        // validando nulidade do objeto
        assertNull(u3);
        assertNotNull(u2);

        // validando se objetos diferentes possuem a mesma instância
        assertSame(u4, u1);
        assertNotSame(u1, u2);

        // ASSERTTHAT
        // comparação genérica
        assertThat(u1.getNome(), is("Alôncio"));
        // negativa
        assertThat(u2.getNome(), not("Onofre"));


    }



}
