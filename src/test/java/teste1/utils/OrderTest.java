package teste1.utils;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrderTest {
    private static int iCount;
    /*
    * JUnit não garante que a ordem de execução dos métodos de testes seja a mesma de sua declaração.
    * para casos de teste que esteja dependentes, pode-se ordenar a execução do métodos através
    * da annotation @FixMethodOrder, pelo parâmetro MethodSorters.NAME_ASCENDING,
    * de forma que serão ordenado por seus nomes em ordem alfabética.
    * Lembrando que a anotação dar-se-á sobre a Classe.*/

    @Test
    public void testA() {
        iCount++;
    }
    @Test
    public void testB() {
        Assert.assertEquals(1, iCount);
    }
}
