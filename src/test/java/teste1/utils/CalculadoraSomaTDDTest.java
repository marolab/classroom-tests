package teste1.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import teste1.exceptions.DivisaoPorZeroException;
import teste1.servicos.CalculadoraService;

public class CalculadoraSomaTDDTest {

    private CalculadoraService calculadoraService;
    CalculadorSomaTDD calculadorSomaTDD;

    @Before
    public void setUp() {
        calculadoraService = new CalculadoraService();
        calculadorSomaTDD = new CalculadorSomaTDD();
    }

    @Test
    public void somarDoisValores() {
        // cenário
        int experado = 10;
        calculadorSomaTDD.setA(4);
        calculadorSomaTDD.setB(6);
        // ação
        int resultado = calculadoraService.somar(calculadorSomaTDD);
        // validação
        Assert.assertEquals(experado, resultado);

    }

    @Test
    public void subtrairDoisValores() {
        // cenário
        int experado = 4;
        calculadorSomaTDD.setA(6);
        calculadorSomaTDD.setB(2);
        // ação
        int resultado = calculadoraService.subtrair(calculadorSomaTDD);
        // verificação
        Assert.assertEquals(experado, resultado);
    }

    @Test
    public void dividirDoisValores() throws DivisaoPorZeroException {
        // cenário
        int experado = 2;
        calculadorSomaTDD.setA(10);
        calculadorSomaTDD.setB(5);
        // ação
        int resultado = calculadoraService.dividir(calculadorSomaTDD);
        // verificação
        Assert.assertEquals(experado, resultado);

    }

    @Test(expected = DivisaoPorZeroException.class)
    public void dividirDoisValoresException() throws DivisaoPorZeroException {
        // cenário
        calculadorSomaTDD.setA(9);
        calculadorSomaTDD.setB(0);
        // ação + verificação (método elegante)
        calculadoraService.dividir(calculadorSomaTDD);

    }
}
