package teste1.servicos;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import teste1.dao.LocacaoDAO;
import teste1.dao.LocacaoDAOImplements;
import teste1.entidades.Filme;
import teste1.entidades.Locacao;
import teste1.entidades.Usuario;
import teste1.exceptions.FilmeSemEstoqueException;
import teste1.exceptions.LocadoraException;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static teste1.builders.FilmeBuilder.filmeBuilder;
import static teste1.builders.UsuarioBuilder.usuarioBuilder;

// para executar uma classe que não esteja no padrão de execução do JUnit, então anota-se a mesma com RunWith
@RunWith(Parameterized.class)
public class CalculoDescontoLocacaoParametrized {
    // InjectMocks anota a classe principal a ser testada
    @InjectMocks
    private LocacaoService bean;
    // as demais dependências são anotadas por @Mock, as quais serão injetadas na bean principal
    @Mock
    private LocacaoService service;
    @Mock
    private SPCService spcService;
    @Mock
    public static LocacaoDAO locacaoDAO;
    @Mock
    public static Usuario user;

    /* VARIÁVEIS DE INSUMO - PARAMETRO DE PARAMETROS */
    // insumos genérios
    // para também informar ao JUnit quais as variáveis que alimentarão os parâmetros anotados abaixo, também tem de anotá-las com @Parameter (singular)
    // como de forma geral, são mais de uma variável, é importante enumera-las, anotando-as com valor da ordem correspondente que cada uma assumirá no Parameters (plural)
    @Parameterized.Parameter // zero default, a primeira
    public List<Filme> filmes; // variável parâmetro 1


    @Parameterized.Parameter(value=1) // segundo variável parâmetro
    public Double valor;

    @Parameterized.Parameter(value=2) // terceiro parâmetro
    public String cenario;

    /* MASSA DE DADOS - INSUMO DOS PARÂMETROS */
    // variáveis de insumo para as listas
    private static Filme filme1 = filmeBuilder().getFilme();
    private static Filme filme2 = filmeBuilder().getFilme();
    private static Filme filme3 = filmeBuilder().getFilme();
    private static Filme filme4 = filmeBuilder().getFilme();
    private static Filme filme5 = filmeBuilder().getFilme();
    private static Filme filme6 = filmeBuilder().getFilme();

    /* MASSA DE ENTRADA */
    // massa de dados para insumo do teste
    // para informar ao JUnit que estes serão os parâmetros, deve-se anotar o mesmo com @Parameters
    @Parameterized.Parameters(name = "Teste {index} = {2}")
    public static Collection<Object[]> getParametros() {
        return Arrays.asList(
                // CADA LINHA SERÁ UM CENÁRIO, ISTO É, CADA VEZ QUE OS MÉTODOS DE TESTES SERÃO INVOCADOS
                new Object[][] {
                        // cada linha desta matriz representará um cenário distinto
                        {Arrays.asList(filme1, filme2), 8d, "2 filmes - zero desconto"},
                        {Arrays.asList(filme1, filme2, filme3), 11.00, "3 filmes - 25% desconto"},
                        {Arrays.asList(filme1, filme2, filme3, filme4), 14.00, "4 filmes - 50% desconto"},
                        {Arrays.asList(filme1, filme2, filme3, filme4, filme5), 17.00, "5 filmes - 75% desconto"},
                        {Arrays.asList(filme1, filme2, filme3, filme4, filme5, filme6), 20d, "6 filmes - 100% desconto"}
                }
        );

    }

    @Before
    public void setUp() {
        // para que as anotações funcionem fazerdo as Injeções de dependências
        // necessita-se no @Before chamar o método INITMOCKS, passando por parâmetro a classe principal a ser testada.
        MockitoAnnotations.initMocks(this);
        // uma vez chamado o initMocks, não se faz mais necessárias as instanciações e injeções manuais.

        user = usuarioBuilder().getUsuario();

        // OBJETO FAKE - LocacaoDAO locacaoDAO = new LocacaoDAOImplements();
        // fazendo injeção do DAO de teste para o DAO verdadeiro

        // instanciando objeto com Mockito
    }

    // a quantidade de vezes que este teste será chamado é a informada na quantidade de PARAMETERS
    @Test
    public void calcularDescontoConformeNumeroFilmesLocado() throws FilmeSemEstoqueException, LocadoraException {
        // ação
        Locacao resultado = bean.alugarFilme(user, filmes);
        // verificação
        assertThat(resultado.getValor(), is(valor));
    }

    @Test
    public void imprimeValidandoNumeroVezesExecucao() {
        System.out.println("executou");
    }


}
