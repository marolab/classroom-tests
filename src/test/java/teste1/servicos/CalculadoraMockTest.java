package teste1.servicos;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

public class CalculadoraMockTest {

    @Mock
    private CalculadoraService calculadoraServiceMock;

    @Spy
    private CalculadoraService calculadoraServiceSpy;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void teste() {

        // WHEN + ANY -> Em métodos que possuem mais de um parâmetro e é utilizado o Matcher, nos demais também será necessário usar.
        Mockito.when(calculadoraServiceMock.somar(Matchers.anyInt(), Matchers.anyInt())).thenReturn(30);
        Assert.assertEquals(30, calculadoraServiceMock.somar(1, 3));


        // EQ() - Em caso de métodos com mais de um parâmetro e se faz necessária a utilização de Matcher, porém em um dos
        // argumentos é necessário um valor definido, então usa-se EQ (igual)
        // Aqui ficaria: quando chamar o método subtrair, passando qualquer inteiro e outro valor equivalente a zero, então retorne zero.
        Mockito.when(calculadoraServiceMock.subtrair(Matchers.anyInt(), Mockito.eq(0))).thenReturn(1000);
        Assert.assertEquals(1000, calculadoraServiceMock.subtrair(90, 0));

    }

    @Test
    public void argumentCaptureTest() {
        // instancia o capturador de argumento definindo o tipo de classe que será capturada
        ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);
        // capturando os dois argumentos
        Mockito.when(calculadoraServiceMock.somar(argumentCaptor.capture(), argumentCaptor.capture())).thenReturn(5);
        // realiza a verificação de forma comum
        Assert.assertEquals(5, calculadoraServiceMock.somar(851, -273));
        // imprime os valores dos argumentos capturados
        System.out.println(argumentCaptor.getAllValues());
    }

    @Test
    public void differencesMockSpy() {
        // expectativas
        // mock
        Mockito.when(calculadoraServiceMock.somar(1, 2)).thenReturn(8);
        // spy
        Mockito.when(calculadoraServiceSpy.somar(1, 2)).thenReturn(8);
        // cenário em que os argumentos não são iguais aos esperados
        System.out.println("com Mock: " + calculadoraServiceMock.somar(1, 5)); // Mock retorna valor padrão
        System.out.println("com Spy: " + calculadoraServiceSpy.somar(1, 5)); // Spy retorna o cálculo literal, considerano os argumentos

    }

    @Test
    public void forcandoMockChamarMetodoReal() {
        /* THE_CALL_REAL_METHOD */
        // com este método no When, é possível forçar o Mock a executar o valor literal do método.
        Mockito.when(calculadoraServiceMock.somar(1, 6)).thenCallRealMethod();
        // porém, somente funcionará caso haja igualdado dos valores esperados com os literais, caso contrário retornará o padrão, zero.
        System.out.println(calculadoraServiceMock.somar(1, 6));
    }

    @Test
    public void diferencaPadraoMockSpy() {
        // em um método VOID o MOCK não executará o mesmo, não acessará o seu conteúdo
        System.out.println("Mock: ...");
        calculadoraServiceMock.imprimeTest();
        System.out.println("Spy: ...");
        calculadoraServiceSpy.imprimeTest();
    }

    @Test
    public void spyNaoExecutandoMetodo_doNothing() {
        // DO_NOTHING diz ao Mockito para não fazer NADA quando o determinado método for chamado
        System.out.println("Spy...");
        Mockito.doNothing().when(calculadoraServiceSpy).imprimeTest();

    }

    @Test
    public void doReturn_OrdemPrecedenciaExecucao() {
        /* DO_NOTHING */
        // para alterar a ordem de precedência de execução do compilador usando Mock, usamos o Do_Nothing
        // trans: retorne valor (5) quando somar (um mais dois)
        Mockito.doReturn(5).when(calculadoraServiceSpy.somar(1, 2));
        System.out.println("Spy...");

    }
}
