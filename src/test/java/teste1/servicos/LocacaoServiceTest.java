package teste1.servicos;

import org.junit.*;
import org.junit.rules.ErrorCollector;
import org.junit.rules.ExpectedException;
import org.mockito.*;
import teste1.builders.FilmeBuilder;
import teste1.builders.LocacaoBuilder;
import teste1.builders.UsuarioBuilder;
import teste1.dao.LocacaoDAO;
import teste1.dao.LocacaoDAOImplements;
import teste1.entidades.Filme;
import teste1.entidades.Locacao;
import teste1.entidades.Usuario;
import teste1.exceptions.FilmeSemEstoqueException;
import teste1.exceptions.LocadoraException;
import teste1.matchers.DiaSemanaMatcher;
import teste1.utils.DataUtilsLc;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static teste1.builders.FilmeBuilder.filmeBuilder;
import static teste1.builders.LocacaoBuilder.LocacaoBuilder;
import static teste1.builders.UsuarioBuilder.usuarioBuilder;
import static teste1.matchers.MatchersProprio.*;
import static teste1.utils.ErrorMessage.*;

public class LocacaoServiceTest {

    private int count;
    private static int countStatic;
    // as anotações de InjectMock e @Mock, substituiem as injeções de dependências e instanciação
    @InjectMocks
    private LocacaoService bean;
    @Mock
    private Usuario user;
    @Mock
    private LocacaoDAO locacaoDAO;
    @Mock
    private SPCService spc;
    @Mock
    private EmailService emailService;
    @Mock
    private Filme filme;
    @Mock
    private List<Filme> filmes;

    // esta é uma classe para criar regras no Junit
    @Rule
    public ErrorCollector error = new ErrorCollector();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        // no @Before chama-se o método InitMocks para fazer valer as anotações
        MockitoAnnotations.initMocks(this);

        System.out.println("v before");

        // o JUnit trata cada método de forma independente, de maneira que o método anotado com Before
        // irá reiniciar as variáveis ou valores a antes de cada método, não persistindo para todos.
        count++;
        System.out.println("Número de testes: " + count + " com variável comum.");

        countStatic++;
        System.out.println("Número de testes: " + countStatic + " com variável STATIC.");

        filmes = new ArrayList<>();
/*
        Uma vez feitas as anotações @InjectMock e @Mock estas declarações de injeções e instâncias não são mais necessárias

        bean = new LocacaoService();
        user = usuarioBuilder().getUsuario();
        filmes = new ArrayList<>();

        // OBJETO FAKE -  LocacaoDAO locacaoDAO = new LocacaoDAOImplements();
        // fazendo injeção do DAO de teste para o DAO verdadeiro

        // o Mockito é usado para instanciar um objeto não real
        locacaoDAO = Mockito.mock(LocacaoDAOImplements.class);
        bean.setLocacaoDAO(locacaoDAO);
        // injeção por Mock
        spc = Mockito.mock(SPCServiceImplements.class);
        bean.setSPCService(spc);
        // injeção de dependência
        emailService = Mockito.mock(EmailService.class);
        bean.setEmailService(emailService);

 */
    }

    @After
    public void tearDown() {
        System.out.println("^ after");
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("v beforeClass");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("^ afterClass");
    }

    // f I (independent-isoladed) r s t
    @Test
    public void deveLocarFilmeComSucesso() throws Exception {
        // usa o ASSUME para não locar em dia que não seja útil
        Assume.assumeFalse(DataUtilsLc.verificarDiaSemana(new Date(), Calendar.SATURDAY));

        // cenário
        filmes.add(filmeBuilder().getFilme());
        filmes.add(filmeBuilder().getFilme());

        // altero comportamento do SPCService com o Mockito
        Mockito.when(spc.possuiNegativacao(user)).thenReturn(false);
        // ação
        Locacao locacao = bean.alugarFilme(user, filmes);
        // auto-verifica (self-verifying)
        // substitui-se a asserThat pelo error.checkThat
        error.checkThat(locacao.getValor(), not(5.0));
        error.checkThat(DataUtilsLc.isMesmaData(locacao.getDataLocacao(), new Date()), is(true));
        error.checkThat(DataUtilsLc.isMesmaData(locacao.getDataRetorno(), DataUtilsLc.obterDataComDiferencaDias(1)), is(true));

        error.checkThat(locacao.getDataLocacao(), ehHoje());
        error.checkThat(locacao.getDataRetorno(), ehHojeComDiferencaDias(1));

    }
    // Forma ELEGANTE de teste Exceção
    // Ao executar um teste que espera uma exceção com a anotação com expect, não necessita asserções, basta executá-lo
    @Test(expected = FilmeSemEstoqueException.class)
    public void naoDeveLocarFilmeSemEstoque_ExceptionElegante() throws Exception {
        // cenário
        filmes.add(FilmeBuilder.filmeBuilderSemEstoque().getFilme());
        // ação
        bean.alugarFilme(user, filmes);
    }

    // outra forma fazer testes que espera uma exceção, é tratar com assertions o resultado da exceção
    // nesta caso, não lança-se a exceção, mas envolve no try catch.
    @Test
    public void naoDeveLocarFilmesSemEstoque_Exception() {
        // cenário
        filmes.add(filmeBuilder().getFilmeSemEstoque().getFilme());
        filmes.add(filmeBuilder().getFilme());

        // ação
        try {
            bean.alugarFilme(user, filmes);
            // resultado
            Assert.fail(MSG_FAIL_TEST.toString());
        } catch (Exception exception) {
            // resultado
            Assert.assertThat(exception.getMessage(), is(MSG_ERROR_FILME_SEM_ESTOQUE.toString()));
        }
    }
    // ASSERT FAIL
    @Test
    public void naoDeveLocarFilmeSemEstoque_Falha() {
        // entrada
        filmes.add(filmeBuilder().getFilmeSemEstoque().getFilme());
        filmes.add(filmeBuilder().getFilmeSemEstoque().getFilme());

        // ação
        try {
            // este método testa a exceção, caso ocorre um FALSO POSITIVO
            // então utiliza-se um FAIL, caso as condições não passem pela exceção
            // e o método seja executado até o final "sem falhas"
            bean.alugarFilme(user, filmes);
            // resultado
            Assert.fail(MSG_FAIL_TEST.toString());
        } catch (Exception exception) {
            // resultado
            Assert.assertThat(exception.getMessage(), is(MSG_ERROR_FILME_SEM_ESTOQUE.toString()));
        }
    }
    // FORMA RECENTE - utilizando Rule ExpectedException
    // Nesta forma, lança-se a exceção, porém as assertivas de erro ocorrem pelo programado
    // tratando com asserts, através de RULE e não pelo framework
    @Test
    public void naoDeveLocarFilmeSemEstoque_ExceptionRule() throws FilmeSemEstoqueException, LocadoraException {
        System.out.println("forma NOVA (RULE) - início");
        // entrada
        // cenário
        filmes.add(filmeBuilder().getFilmeSemEstoque().getFilme());

        // resultado - validação
        // nesta caso, como deve-se informar ao JUnit o que se espera
        // a validação ficará anterior à execução, como se fizesse parte do cenário de entrada.
        expectedException.expect(Exception.class); // verifica se recebe a exceção esperada
        expectedException.expectMessage(MSG_ERROR_FILME_SEM_ESTOQUE.toString()); // verfica a mensagem gerada pela exceção

        //ação
        bean.alugarFilme(user, filmes);

    }
    // testando da maneira Robusta, sem o framework, envolvendo em Try-Catch
    // Neste caso, a exceção de usuário está na forma Robusta, e a de Filme sem estoque como Elegante.
    @Test
    public void naoDeveLocarFilmeSemUsuario_ExceptionRobusta() throws FilmeSemEstoqueException { // forma ELEGANTE
        System.out.println("forma ROBUSTA - início");
        // cenário (sem usuário)
        filmes.add(filmeBuilder().getFilme());

        // ação - sem usuário - forma ROBUSTA
        try {
            bean.alugarFilme(null, filmes);
            // para a forma Robusta, faz-se necessário o FAIL
            Assert.fail();
        } catch (LocadoraException e) {
            Assert.assertThat(e.getMessage(), is(MSG_ERROR_USUARIO_NAO_INFORMADO.toString()));
        }
        System.out.println("forma ROBUSTA - fim");
    }
    // forma NOVA (lança as exceções, porém valida dentro do método, não pelo framework, através de RULE
    @Test
    public void naoDeveLocarFilmeSemInformarFilme_NovaException() throws FilmeSemEstoqueException, LocadoraException {
        System.out.println("forma NOVA - início");

        // Cenário (sem filme)

        // resultado - verificação vem antes da ação
        expectedException.expect(LocadoraException.class);
        expectedException.expectMessage(MSG_ERROR_FILME_NAO_INFORMADO.toString());

        // ação
        bean.alugarFilme(user, null);

    }

    @Test
    public void naoDeveDevolverFilmeNoDomingo() throws FilmeSemEstoqueException, LocadoraException {
        // usando a bibliote ASSUME, é possível impor uma condição para que o teste seja executado ou não
        Assume.assumeTrue(DataUtilsLc.verificarDiaSemana(new Date(), Calendar.SATURDAY));

        // cernário
        filmes.add(filmeBuilder().getFilme());
        // ação
        Locacao retorno = bean.alugarFilme(user, filmes);
        //verificação
        boolean isMonday = DataUtilsLc.verificarDiaSemana(retorno.getDataRetorno(), Calendar.MONDAY);
        Assert.assertTrue(isMonday);
        // usando um Matcher próprio (parâmetro - integer - dia da semana)
        Assert.assertThat(retorno.getDataRetorno(), new DiaSemanaMatcher(Calendar.MONDAY));
        // outro Matcher próprio, agora encapsulando a classe de Matcher então criada
        Assert.assertThat(retorno.getDataRetorno(), caiEm(Calendar.MONDAY));
        // mais encapsulamento
        Assert.assertThat(retorno.getDataRetorno(), caiNumaSegunda());
    }
    // utilizando a forma Robusta de tratar exceção para controlar melhor as exceções.
    @Test
    public void naoDeveLocarFilmeUsuarioNegativadoSPC() throws Exception {
        // cenário
        Usuario usuario = usuarioBuilder().getUsuario();

        filmes.add(filmeBuilder().getFilme());

        // DEFININDO COMPORTAMENTO
        // com o Mockito, é possível definir o comportamento do objeto Mockado.
        Mockito.when(spc.possuiNegativacao(usuario)).thenReturn(true);

        // WHEN + ANY - usando o ANY (qualquer) em um WHEN (expectativa)
        Mockito.when(spc.possuiNegativacao(Matchers.any(Usuario.class))).thenReturn(true);

        // tratando exceções de forma ROBUSTA
        try {
            // ação no final
            bean.alugarFilme(usuario, filmes);
            // força a falha caso o método funcione

            // verificação começa aqui...
            Assert.fail();
        } catch (LocadoraException e) {
            // como se trata do método rescente, utilizando RULES (Exception)
            // a verificação (esperado) antes da ação
        //    expectedException.expect(LocadoraException.class);
        //    expectedException.expectMessage(MSG_ERROR_USUARIO_NEGATIVADO.toString());
            Assert.assertThat(e.getMessage(), is(MSG_ERROR_USUARIO_NEGATIVADO.toString()));

        }

        // verificação (utilizando VERIFY - comportamento de método)
        Mockito.verify(spc).possuiNegativacao(usuario);
    }

    @Test
    public void deveEnviarEmailParaLocacoesEmAtraso(){
        List<Usuario> usuarios = Arrays.asList(
                usuarioBuilder().getUsuario(),
                usuarioBuilder().comNome("Snelson").getUsuario(),
                usuarioBuilder().comNome("Osmar Telão").getUsuario(),
                usuarioBuilder().comNome("Onofre").getUsuario(),
                usuarioBuilder().comNome("Ambrosio").getUsuario()
        );

        // cenário com 3 locações usando Builder
        List<Locacao> locacaoList = Arrays.asList(
                // NEVER
                LocacaoBuilder().comUsuario(usuarios.get(0)).getLocacao(),
                // usuário em atraso
                LocacaoBuilder().comUsuario(usuarios.get(1)).atrasados().getLocacao(),
                // duas interações para o mesmo usuário (TIMES)
                LocacaoBuilder().comUsuario(usuarios.get(2)).atrasados().getLocacao(),
                LocacaoBuilder().comUsuario(usuarios.get(2)).atrasados().getLocacao(),
                LocacaoBuilder().comUsuario(usuarios.get(3)).atrasados().getLocacao(),
                LocacaoBuilder().comUsuario(usuarios.get(3)).atrasados().getLocacao(),
                LocacaoBuilder().comUsuario(usuarios.get(3)).atrasados().getLocacao(),
                LocacaoBuilder().comUsuario(usuarios.get(4)).atrasados().getLocacao()

        );
        // comportamento mockado retornando a lista criada no cenário
        Mockito.when(locacaoDAO.obterLocacoesPendentes()).thenReturn(locacaoList);

        // ação
        bean.notificarAtrasos();
        // verificação da ação
        // O método VERIFY, tem como objetivo, verificar o COMPORTAMENTO do método da classe a ser testada
        Mockito.verify(emailService).notificarAtraso(usuarios.get(1)); // neste teste o usuário 1 não poderá receber email, somente o Usuário 2

        // fazendo a verificação contrária, isto é, que tal comportamento não poderá acontecer
        // informa-se no VERIFY, por parâmetro que não poderá ocorrer (Mockito.never)
        Mockito.verify(emailService, Mockito.never()).notificarAtraso(usuarios.get(0));

        // TIMES - para verificar métodos que terão mais de uma interação, utiliza-se o Mockito.TIMES(número de interações)
        Mockito.verify(emailService, Mockito.times(2)).notificarAtraso(usuarios.get(2));

        // AT_LEAST - quando não se conhece o número exato de interações, usa-se o Mockito.atLeast(número mínimo de interações)
        // isto quer dizer, no mínimo X interações.
        Mockito.verify(emailService, Mockito.atLeast(3)).notificarAtraso(usuarios.get(3));

        // AT_LEAST_ONCE - se executar pelo menos UMA VEZ já será o suficiente para a verificação
        Mockito.verify(emailService, Mockito.atLeastOnce()).notificarAtraso(usuarios.get(3));

        // AT_MOST - quando se quer limitar o número máximo de interações que deverá acontecer.
        Mockito.verify(emailService, Mockito.atMost(1)).notificarAtraso(usuarios.get(4));

        // MATCHER.ANY - com este matcher.ANY em um VERIFY, validará se o método foi executado se qualquer valor
        // (sem ser específico) foi passado.
        Mockito.verify(emailService, Mockito.atLeastOnce()).notificarAtraso(Matchers.any(Usuario.class));

        // VERIFY_NO_MORE_INTERACTIONS
        // Como uma verificação ainda mais restritiva
        // com este comando é possível ter certeza que nenhuma outra interação ocorrerá a mais que que já foi verificado
        // para esta classe (método).
        Mockito.verifyNoMoreInteractions(emailService);

        // VERIFY_ZERO_INTERACTIONS
        // Com esta verificação é possível certificar-se que NENHUMA interação ocorreu (neste caso testamos com outro objeto
        // não comum ao contexto deste teste.
        Mockito.verifyZeroInteractions(spc);

    }

    @Test
    public void testarSPCFora() throws Exception {
        // cenário
       filmes.add(FilmeBuilder.filmeBuilder().getFilme());
        // - expectativa
        Mockito.when(spc.possuiNegativacao(Matchers.any(Usuario.class))).thenThrow(new Exception(MSG_ERROR_SPC_FORA_DO_AR.toString()));
        // usando RULES (forma mais NOVA)

        // verificação (sobe na expectativa da forma de Rules (nova)
        expectedException.expect(LocadoraException.class);
        expectedException.expectMessage(MSG_ERROR_SPC_FORA_DO_AR.toString());

        // ação
        bean.alugarFilme(UsuarioBuilder.usuarioBuilder().getUsuario(), filmes);
    }

    @Test
    public void prorrogarLocacaoTest() {
        // cenário
        Locacao locacao = LocacaoBuilder().getLocacao();
        // ação
        bean.prorrogarLocacao(locacao, 3);
        // verificação
        /* Com ARGUMENTO_CAPTURE, é possível capturar o valor do argumento passado para um método, ao chamá-lo */
        // defini-se o tipo que será o ArgumentCaptor <Classe.class>
        ArgumentCaptor<Locacao> argumentCaptor = ArgumentCaptor.forClass(Locacao.class);
        // salvando o argumento passado
        Mockito.verify(locacaoDAO).salvar(argumentCaptor.capture());
        // usa-se o que está salvo no ArgumentCaptor
        Locacao locacaoCapturada = argumentCaptor.getValue();

        // verifica-se
        error.checkThat(locacaoCapturada.getValor(), is(12.0));
        error.checkThat(locacaoCapturada.getDataLocacao(), ehHoje());
        error.checkThat(locacaoCapturada.getDataRetorno(), ehHojeComDiferencaDias(3));

    }

    //=========================================== IGNORE =========================================================== IGNORE ========================================//
    // todos os métods abaixo serão anotados com IGNORE, pois eles serão validados na outra classe
    // usando PARAMETERIZED
    @Test
    @Ignore
    public void devePagar75pctNoFilme3() throws FilmeSemEstoqueException, LocadoraException {
        // cenário
        List<Filme> filmes = Arrays.asList(
                filmeBuilder().getFilme(),
                filmeBuilder().getFilme(),
                filmeBuilder().getFilme());
        // ação
        Locacao resultado = bean.alugarFilme(user, filmes);
        // verificação
        Assert.assertThat(resultado.getValor(), is(11.0));

    }

    @Test
    @Ignore
    public void devePagar50pctNoFilme4() throws FilmeSemEstoqueException, LocadoraException {
        // cenário
        List<Filme> filmes = Arrays.asList(
                filmeBuilder().getFilme(),
                filmeBuilder().getFilme(),
                filmeBuilder().getFilme(),
                filmeBuilder().getFilme()
        );
        // ação
        Locacao resultado = bean.alugarFilme(user, filmes);
        // verificação
        Assert.assertThat(resultado.getValor(), is(14.0));

    }

    @Test
    @Ignore
    public void devePagar25pctNoFilme5() throws FilmeSemEstoqueException, LocadoraException {
        // cenário
        for(int i = 0; i <=4; i++) filmes.add(filmeBuilder().getFilme());
        // ação
        Locacao resultado = bean.alugarFilme(user, filmes);
        // verificação
        Assert.assertThat(resultado.getValor(), is(17.0));

    }

    @Test
    @Ignore
    public void devePagar0pctMaisFilme5() throws FilmeSemEstoqueException, LocadoraException {
        // cenário
        for(int i = 0; i <=5; i++) filmes.add(filmeBuilder().getFilme());
        // ação
        Locacao resultado = bean.alugarFilme(user, filmes);
        // verificação
        Assert.assertThat(resultado.getValor(), is(20.0));

    }

}